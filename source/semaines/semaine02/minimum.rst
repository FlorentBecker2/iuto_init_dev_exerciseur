Minimum
=======

Ecrire une fonction renvoie le plus petit nombre d'une liste de nombres. 



.. easypython:: ./minimum/
   :language: Jacadi
   :titre: minimum
   :extra_yaml:
     fichier_ens: minimum.py
