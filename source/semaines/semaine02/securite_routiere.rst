Sécurité routière
-----------------

D'après le site http://www.controleradar.org/contraventions.html voici la
réglementation en terme de contravention en cas d'excès de vitesse.

- Contravention pour dépassement de vitesse inférieur à 20 km/h

  - zones où la vitesse limitée est supérieure à 50 km/h
    
    Amende: 68 euros, Retrait 1 point, Suspension de permis : aucune

  - vitesse limitée inférieure à 50 km/h
    
    Amende : 135 euros, Retrait 1 point, Suspension de permis : aucune

- Contravention pour dépassement de vitesse entre 20 km/h et 30 km/h
  
  Amende: 135 euros, Retrait 2 points, Suspension de permis: aucune
  
- Contravention pour dépassement de vitesse entre 30 km/h et 40 km/h
  
  Amende  135 euros, Retrait 3 points, Suspension de permis: 3 ans
  
- Contravention pour dépassement de vitesse entre 40 km/h et 50 km/h
  
  Amende  135 euros, Retrait 4 points, Suspension de permis: 3 ans
  
- Contravention pour dépassement de vitesse de plus de 50 km/h
  
  Amende de 1500 euros, Retrait 6 points, Suspension de permis : 3 ans

On souhaiterait avoir une fonction qui nous donne les sanctions
encourues en fonction de l'excès de vitesse que l'on a commis.


.. easypython:: ./exosecu-routiere/
   :language: Jacadi
   :titre: secu routiere
   :extra_yaml:
     fichier_ens: exosecu-routiere.py


