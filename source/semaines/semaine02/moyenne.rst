Moyenne
=======

Ecrire une fonction renvoie la moyenne des nombres dans une liste de nombres. 


.. easypython:: ./moyenne/
   :language: Jacadi
   :titre: moyenne
   :extra_yaml:
     fichier_ens: moyenne.py
