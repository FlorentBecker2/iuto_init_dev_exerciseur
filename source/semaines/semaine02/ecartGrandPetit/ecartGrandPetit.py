entrees_visibles = [[1,2,3,4,5,6,7,8,9,10],
                    [15,4,6,-5,6,10],
                    [18],
]
entrees_invisibles = [
                    [-202,24,14,8,-19],
                    [-9999999999],
                    [-9999999999, -555555555555],
]

@solution
def ecartGrandPetit(liste) :
  return max(liste)-min(liste)
