Nombre occurrences lettre
=========================

Ecrire une fonction renvoie Le nombre de fois où une lettre apparait dans un mot



   
.. easypython:: ./nbApparition_str/
   :language: Jacadi
   :titre: nbApparition str
   :extra_yaml:
     fichier_ens: nbApparition_str.py
