entrees_visibles = [('a','programmation'),
                    ('z','programmation'), 
                    ('a',''),                
]
entrees_invisibles = [
                    ('p','programmation'), 
                    ('n','programmation'),                  
                    ('m','programmation'),
]

@solution
def compte(lettre, mot) :
  res=0
  for l in mot:
    if l==lettre:
      res+=1
  else:
    return res
