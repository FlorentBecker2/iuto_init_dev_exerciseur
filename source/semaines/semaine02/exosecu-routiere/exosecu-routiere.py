entrees_visibles = [(35,30),
                    (80,70),
                    (200,130),
                    (90,110)
]
entrees_invisibles = [
        (45,30),
        (160,120),
        (90,60)
]

@solution
def contravention(vitesse_mesuree, limite_vitesse) :
    """
    fonction permettant de connaitre en fonction de la vitesse mesurée et 
    de la limite de vitesse autorisée quelle est la contravention : 
    nbre de points retirés, somme due et duree suspension.
    """

    depassement=vitesse_mesuree-limite_vitesse
    points = 0
    duree = "aucune"
    somme = 0
    if depassement > 0 :
        if depassement < 20 :
            if limite_vitesse <= 50 :
                points = 1
                somme = 135
            else :
                # à plus de 50 km/h
                points = 1
                somme = 68
        else :
            #depassement >= 20
            if depassement < 30 :
                points = 2
                somme = 135
            else :
                duree = "3 ans"
                if depassement < 40 :
                    points = 3
                    somme = 135
                else :
                    if depassement < 50 :
                        points = 4
                        somme = 135
                    else :
                        points = 6
                        somme = 1500
    euro="euro"
    if somme>1:
        euro+="s"
    pt="point"
    if points>1:
        pt+="s"
    res = "Amende : "+str(somme)+" "+euro+", retrait : "+str(points)+" "+pt+", suspension de permis : "+duree
    return res
