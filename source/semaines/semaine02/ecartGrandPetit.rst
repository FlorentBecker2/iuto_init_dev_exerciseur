Ecart entre le plus grand et le plus petit
==========================================

Ecrire une fonction renvoie l'écart entre le plus grand nombre et le plus petit nombre  dans une liste. 


.. easypython:: ./ecartGrandPetit/
   :language: Jacadi
   :titre: ecart grand petit
   :extra_yaml:
     fichier_ens: ecartGrandPetit.py
