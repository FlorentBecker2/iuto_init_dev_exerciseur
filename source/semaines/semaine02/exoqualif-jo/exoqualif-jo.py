entrees_visibles = [('H',12,2,True),
                    ('H',12,2,False),
                    ('F',14,3,False)
]
entrees_invisibles = [('H',14,5,False),
                      ('F',52,0,True)
]

@solution
def qualification(sexe, record, nb_courses_gagnees, champion_du_monde):
    """
    fonction permettant de décider si une personne est 
    qualifiée aux JO en fonction des critères émis par la
    fédération
    paramètres: sexe un caractère 'H' pour homme 'F' pour femme
                record un float indiquant le record personnel de la personne
                nb_courses_gagnees un entier indiquant le nombre de courses gagnees dans l'année
                champion_du_monde un booléen indiquant si la personne est championne du monde ou non
    résultat: un booléen indiquant si la personne est qualifiée ou non pour les JO
    """
    res=False
    if champion_du_monde:
        res=True
    else: # La personne n'est pas championne du monde
        if nb_courses_gagnees>=3: #homme ou femme il faut avoir gagné au moins 3 courses
            if sexe=='H' and record<=12: # critère hommes
                res=True
            elif sexe=='F' and record<=15: #critère femmes
                res=True
    return res
