entrees_visibles = [[1,102,365,14,201,8,471,365],
                    [1,102,365,14,21,8,365],
                    [102,586,456],
]
entrees_invisibles = [
        [],
        [14,23,10,8,7],
        [147,2,6,89],
]

@solution
def quatreSup100(listeNombres):
    """
    paramètres: listeNombres: une liste de nombres
    résultat: la liste des quatre premiers éléments supérieurs
    (ou égaux) à 100 de listeNombre
    """ 
    res=[]
    i=0
    while i<len(listeNombres) and len(res)<4:
      if listeNombres[i]>=100:
        res.append(listeNombres[i])
      i+=1
    return res
