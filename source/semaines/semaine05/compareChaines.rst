:orphan:

Exercice 5.2
------------

On veut comparer deux chaines de caractères en utilisant **uniquement** la comparaison entre deux caractères.
La fonction devrat retourner -1 si la première chaine est avant la seconde dans l'ordre alphabétique,
0 si elles sont égales et 1 sinon.

.. easypython:: ./compareChaines/
   :language: Jacadi
   :titre: compareChaines
   :extra_yaml:
     fichier_ens: compareChaines.py
