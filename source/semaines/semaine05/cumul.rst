Cumul debut de liste
--------------------

On aimerait trouver dans une liste l'indice tel que la somme des
éléments du début de la liste jusqu'à cet indice dépasse une certaine
valeur. Par exemple pour la liste [4,5,-3,1,6,2,-3] et la valeur 10 on
veut trouver l'indice 4 car si on fait la somme 4+5-3+1+6 c'est la
première fois que l'on dépasse 10. Si la valeur n'est jamais dépassée,
on doit retourner -1.

.. easypython:: ./cumul/
   :language: Jacadi
   :titre: cumul
   :extra_yaml:
     fichier_ens: cumul.py
