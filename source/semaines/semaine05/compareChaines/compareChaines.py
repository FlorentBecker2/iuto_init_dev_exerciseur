entrees_visibles = [('abeille','abricot'),
                    ('abeille','abeille'),
                    ('abri','abeille'),
                    ('comme','commentaire'),
                    ('commentaire','comme'),

]
entrees_invisibles = [
        ('abc','zbc'),
        ('abricot','abeille'),
        ('abeille','abri'),
]

@solution
def compareChaine(chaine1, chaine2):
    """
    paramètres: deux chaines de caractères
    résultat: si chaine1< chaine2 : -1
              si chaine1=chaine2 : 0
              si chaine1>chaine2 : 1
    """
    if chaine1<chaine2:
      res=-1
    elif chaine1>chaine2:
      res=1
    else:
      res=0
    return res
