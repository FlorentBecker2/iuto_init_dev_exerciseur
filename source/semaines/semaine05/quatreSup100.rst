:orphan:

Exercice 5.1
------------

Écrire une fonction qui constitue la liste des quatre premiers éléments supérieurs (ou égaux) à 100 
dans une liste de nombres.


.. easypython:: ./quatreSup100/
   :language: Jacadi
   :titre: quatreSup100
   :extra_yaml:
     fichier_ens: quatreSup100.py
