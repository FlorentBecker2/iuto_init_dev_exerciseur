entrees_visibles = [2,
                    13,
                    25,
]
entrees_invisibles = [
        125,
        14,
        15,
]

@solution
def sommeNpremiersEntiers(n):
    """
    Fait la somme des premiers entiers
    paramètres: n: le dernier nombre entier
    résultat: la somme n premiers entiers
    """ 

    res=0
    for i in range(n+1):
            res+=i
    return res
