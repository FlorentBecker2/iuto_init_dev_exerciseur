

Somme des nombres paires d'une liste
--------------------------------------

Écrire une fonction qui fait la somme des nombres paires d’une liste
de nombres. Par exemple si la liste est [12,13,6,5,7] le résultat doit
être 18 car seuls 12 et 6 sont paires

.. easypython:: ./somme_paires/
   :language: Jacadi
   :titre: somme_paires
   :extra_yaml:
     fichier_ens: somme_paires.py
