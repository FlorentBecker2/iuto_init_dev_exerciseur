entrees_visibles = [[12,13,6,5,7],
                    [6,2,1,8,6],
                    [1,3,5,7]
]
entrees_invisibles = [
        [],
        [14,23,10,8,7],
        [-4,1,-4,3,8]
]

@solution
def sommePairs(listeNombres):
    """
    Fait la somme des nombres paires d'une liste
    paramètres: listeNombres: une liste de nombres
    résultat: la somme des nombre paires de la liste
    """ 
    res=0
    for nb in listeNombres:
        if nb%2==0:
            res+=nb
    return res
