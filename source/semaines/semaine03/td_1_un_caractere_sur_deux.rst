Un caractère sur deux
=====================

On voudrait construire une chaîne contenant uniquement un caractère sur *n* d'une autre chaîne. Par exemple pour la chaîne 'bonjour' et le nombre 2 on souhaiterait obtenir la 'bnor' et pour 'bonjour' et le nombre 3, on voudrait 'bjr'. 

.. easypython:: ./un_caractere_sur_deux/
   :language: Jacadi
   :titre: un_caractere_sur_deux
   :extra_yaml:
     fichier_ens: un_caractere_sur_deux.py
