entrees_visibles = ['creee',
                    'coucou',
                    'appeler',
]
entrees_invisibles = [
        'manger',
        'aabcde',
        '',
]

@solution
def consecutifs(mot):
    """
    indique si deux lettres consécutives sont égales dans le mot
    paramètres: m: un mot (une chaine de caractères
    résultat: True ou False suivant qu'on a toruvé ou non
    """ 

    res=False
    prec=''
    for c in mot:
        if c==prec:
            res=True
        prec=c
    return res
