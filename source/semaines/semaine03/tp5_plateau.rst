

Plus grand plateau
-------------------

Écrire une fonction qui permet de connaître dans une liste de nombres,
la longueur de la plus grande suite de nombres consécutifs
égaux. Par exemple sur la liste [6,6,3,4,2,2,2,4] on doit trouver 3
car la plus grande suite contient trois 2 consécutifs.

.. easypython:: ./plateau/
   :language: Jacadi
   :titre: plateau
   :extra_yaml:
     fichier_ens: plateau.py
