entrees_visibles = [2,
                    13,
                    25
]
entrees_invisibles = [
        125,
        14,
        15
]

@solution
def sommeNombresPairs(n):
    """
    Fait la somme des nombres paires parmi les n premiers entiers
    paramètres: n: le dernier nombre entier
    résultat: la somme des nombres paires
    """ 

    res=0
    for i in range(2,n+1,2):
            res+=i
    return res
