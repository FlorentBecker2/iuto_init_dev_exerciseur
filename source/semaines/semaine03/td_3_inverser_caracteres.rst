Inverser caractères
===================

On voudrait construire une chaîne à partir d'une autre en permuttant les 
lettres consécutives deux à deux. 
Par exemple avec la chaîne *'bonjour'* on devrait obtenir *'objnuor'*.  

.. easypython:: ./inverser_caracteres/
   :language: Jacadi
   :titre: inverser_caracteres
   :extra_yaml:
     fichier_ens: inverser_caracteres.py
