

Somme des n premiers entiers
-----------------------------

Écrire une fonction qui fait la somme des n premiers entiers. Par
exemple si n vaut 4 le résultat doit être 10 qui est le résultat de 1+2+3+4.

.. easypython:: ./somme_n_premiers_entiers/
   :language: Jacadi
   :titre: somme_n_premiers_entiers
   :extra_yaml:
     fichier_ens: somme_n_premiers_entiers.py
