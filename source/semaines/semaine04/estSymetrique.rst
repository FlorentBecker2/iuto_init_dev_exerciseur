Liste symétrique
================

\'Ecrire une fonction qui vérifie si une liste est symétrique. 


.. easypython:: ./estSymetrique/
   :language: Jacadi
   :titre: estSymetrique
   :extra_yaml:
     fichier_ens: estSymetrique.py
