entrees_visibles = [1234,
                    0,
]
entrees_invisibles = [
        1,
        12350,
        12000350,
]

@solution
def nombreToListe(nombre):
  """
  """
  liste=[nombre%10]
  while nombre//10!=0:
    nombre=nombre//10
    liste.append(nombre%10)
  return liste
