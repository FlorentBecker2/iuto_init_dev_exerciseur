entrees_visibles = [5,2,8,]
entrees_invisibles = [6,50,7]

@solution
def initCrible(n):
    return [False]*2+[True]*(n-1)
