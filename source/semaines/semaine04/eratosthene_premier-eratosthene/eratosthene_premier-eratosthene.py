entrees_visibles = [10,
                    30,
]
entrees_invisibles = [100]

def initCrible(n):
    return [False]*2+[True]*(n-1)

def multiple(c,x):
    for i in range(x,len(c),x):
        c[i]=False
    return c

@solution
def crible(n):
    res=[]
    c=initCrible(n)
    for x in range(n+1):
        if c[x]:
            res+=[x]
            multiple(c,x)
    return res
