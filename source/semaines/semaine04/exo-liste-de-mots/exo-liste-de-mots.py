entrees_visibles= ["le lundi, c'est le premier jour de la semaine",
                   "3*(4+6)",
                   " Mon numero est le 06060606 !!!",
] 
 
entrees_invisibles = ["",
                      "mot",
                      "une phrase beaucoup plus longue...",
]

@solution
def listeMots(texte):
    """ 
    retourne la liste des mots d'un texte
    paramètre: texte: le texte à traiter
    résultat: la liste des mots du texte
    """
    res=[]
    mot=''
    for lettre in texte:
        if lettre.isalpha():
            mot+=lettre
        else:
            if mot!='':
                res.append(mot)
                mot=''
    if mot!='':
        res.append(mot)
    return res
