entrees_visibles = [("le","le lundi, c'est le premier jour de la semaine"),
                    ("journée","le lundi, c'est le premier jour de la semaine"),
                    ("la","l'air de la chanson fait tralalala!"),
]
entrees_invisibles = [("cours","je cours car c'est la course pour faire les courses"),
                      ("chouette","le lapin est gris"),
                      ("test","")
]
def mot_indice(mot,texte,indice):
   """
   retourne vrai si le mot se trouve dans le texte à l'indice indiqué
   paramètres;
   mot: le mot recherché
   texte: le texte où on fait la recherche
   indice: un entier indiquant l'indice testé
   résultat un booleen
   """
   res=True
   for i in range(len(mot)):
      if mot[i]!=texte[indice+i]:
         res=False
   return res

@solution      
def listeOccurrences(mot, texte):
   """
   retourne la liste des indices où le mot se trouve dans le texte
   paramètres;
   mot: le mot recherché
   texte: le texte où on fait la recherche
   résultat: la liste des indices où le mot apparaît dans le texte
   """
   res=[]
   for i in range(len(texte)-len(mot)+1):
      if mot_indice(mot,texte,i):
         res.append(i)
   return res
