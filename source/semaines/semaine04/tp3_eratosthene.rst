Le crible d'Ératosthène:
========================

Dans cet exercice nous allons implémenter un algorithme qui date du
IIIe siècle avant notre ère. Il s'agit d'un algorithme permettant de
trouver les nombres premiers dans la liste des ``N`` premiers
entiers. L'algorithme est le suivant. On construit une liste de ``N+1``
booléens tous à ``True`` sauf les deux premiers (0 et 1 ne sont pas
des nombres premiers par définition). Ensuite, pour chaque entier ``x``
si le booléen qui porte son indice est ``True``, il est répertorié
comme un nombre premier et tous ses multiples sont mis à ``False``.
Vous pouvez consulter la page https://fr.wikipedia.org/wiki/Crible_d'Ératosthène pour plus de détails.

Pour écrire cet algorithme en Python, nous allons implémenter
plusieurs fonctions

Initialisation
--------------
Écrire une fonction Python qui initialise une liste de ``N+1`` booléens tous à ``True`` sauf les deux premiers.


.. easypython:: ./eratosthene_premier-init/
   :language: Jacadi
   :titre: eratosthene_premier-init
   :extra_yaml:
     fichier_ens: eratosthene_premier-init.py
     
Multiples
---------
Écrire une fonction Python qui pour un entier  ``x`` et une liste met à ``False`` tous les booléens d'indice multiple de 
  ``x`` (sauf celui de ``x`` lui-même).

.. easypython:: ./eratosthene_premier-multiples/
   :language: Jacadi
   :titre: eratosthene_premier-multiples
   :extra_yaml:
     fichier_ens: eratosthene_premier-multiples.py
     
Crible
------
Écrire une fonction qui implémente le crible pour les ``N`` premiers entiers. **Attention** vous devez recopier les deux fonctions précédentes dans le cadre réponse pour les utiliser dans la fonction de cet exercice.

.. easypython:: ./eratosthene_premier-eratosthene/
   :language: Jacadi
   :titre: eratosthene_premier-eratosthene
   :extra_yaml:
     fichier_ens: eratosthene_premier-eratosthene.py
