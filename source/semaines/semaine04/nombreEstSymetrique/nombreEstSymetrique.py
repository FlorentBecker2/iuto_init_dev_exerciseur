entrees_visibles = [12321,
                    3,
                    21,
]
entrees_invisibles = [
        1,
        1221,
        12213,
        123521,
        0,
]

@solution
def nombreEstSymetrique(nombre):
  """
  """
  return estSymetrique(nombreToListe(nombre))


def nombreToListe(nombre):
  """
  """
  liste=[nombre%10]
  while nombre//10!=0:
    nombre=nombre//10
    liste.append(nombre%10)
  return liste

def estSymetrique(liste):
  """
  """ 
  i=0
  fin=len(liste)-1
  symetrique=True
  while symetrique and i<len(liste)/2:
    symetrique = (liste[i]==liste[fin-i])
    i+=1
  return symetrique
