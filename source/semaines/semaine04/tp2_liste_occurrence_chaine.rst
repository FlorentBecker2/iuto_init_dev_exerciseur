
Liste occurrences d'une chaine:
-------------------------------

Écrire une fonction qui prend en paramètres deux chaînes et qui retourne la liste des indices où la première chaîne apparaît dans la seconde. Par exemple, si on recherche ``"le"`` dans  ``"le lundi, c'est le premier jour de la semaine"``, la fonction doit retourner la liste ``[0,16]`` car le mot ``"le"`` apparaît aux positions 0 et 16 de la chaîne considérée.


.. easypython:: ./exo-liste-occurrences-chaine/
   :language: Jacadi
   :titre: exo-liste-occurrences-chaine
   :extra_yaml:
     fichier_ens: exo-liste-occurrences-chaine.py
