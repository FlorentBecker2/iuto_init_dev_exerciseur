entrees_visibles = [(3,12,[12,3,4,12,8,12,5,12]),
                    (3,7,[12,3,4,12,8,12,5,12]),
                    (4,7,[12,3,4,12,8,12,5,12]),
]
entrees_invisibles = [
        (4,7,[]),
        (4,12,[12,3,4,12,8,12,5,12]),
        (5,12,[12,3,4,12,8,12,5,12]),
]

@solution
def niemeOccurrence(n, element, liste):
    """
    renvoie l'indice de la nième occurrence d'un élément d'une liste.
    renvoie -1 si la liste contient moins de n fois l'element
    """ 
    i=0
    cpt=0
    while i<len(liste) and cpt<n:
      if liste[i]==element:
        cpt+=1
      i+=1
    if cpt==n:
      indice=i-1
    else:
      indice=-1
    return indice
