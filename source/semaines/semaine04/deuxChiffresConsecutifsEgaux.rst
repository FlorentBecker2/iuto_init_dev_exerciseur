Deux chiffres consecutifs égaux
===============================

On voudrait savoir si un nombre (positif) contient deux chiffres consécutifs égaux.

\'Ecrire une fonction qui résoud ce problème.

.. easypython:: ./deuxChiffresConsecutifsEgaux/
   :language: Jacadi
   :titre: deuxChiffresConsecutifsEgaux
   :extra_yaml:
     fichier_ens: deuxChiffresConsecutifsEgaux.py
