Nombre symétrique
=================

On voudrait savoir si un nombre (positif) est symétrique ou non.

\'Ecrire une fonction qui résoud ce problème.

.. easypython:: ./nombreEstSymetrique/
   :language: Jacadi
   :titre: nombreEstSymetrique
   :extra_yaml:
     fichier_ens: nombreEstSymetrique.py
