Liste des mots
--------------

Écrire une fonction qui prend en paramètres une chaîne de caractères contenant un texte et qui retourne la liste des mots qui se trouvent dans cette chaîne.
Pour cette fonction, un mot commence par un caractère alphabétique et s'arrête au premier caractère non alphabétique. La méthode ``isalpha()`` permet de tester si une chaîne ne contient que des caractères alphabétiques.

.. easypython:: ./exo-liste-de-mots/
   :language: Jacadi
   :titre: exo-liste-de-mots
   :extra_yaml:
     fichier_ens: exo-liste-de-mots.py
