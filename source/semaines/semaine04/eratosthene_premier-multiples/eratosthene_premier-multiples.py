entrees_visibles = [([False,False,True,True,True,True,True,True],2),
                    ([False,False,True,True,False,True,False,True,False,True],3)
]
entrees_invisibles = [([False,False,True,True,False,True,False,True,False,True,True],5)]
@solution
def multiple(c,x):
    for i in range(x,len(c),x):
        c[i]=False
    return c
