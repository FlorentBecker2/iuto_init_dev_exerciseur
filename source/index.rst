.. EasySphinx documentation master file, created by
   sphinx-quickstart on Thu Oct 20 12:10:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Initiation au développement
================================


.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   :caption: Exercices par semaine
   
   semaines/index*



.. toctree::
   :maxdepth: 1
   :numbered: 0
   :glob:
   :caption: Autres exercices
   
   exercices/index*
