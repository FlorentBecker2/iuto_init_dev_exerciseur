Année bissextile ?
--------------------


D'après Wikipédia, une année est bissextile  :

   * si l'année est divisible par 4 et non divisible par 100, ou
   * si l'année est divisible par 400.


Ainsi, 2017 n'est pas bissextile. L'an 2008 était bissextile (divisible par 4 et non divisible par 100).
L'an 1900 n'était pas bissextile car divisible par 4 mais aussi par 100 et non divisible par 400.
L'an 2000 était bissextile car divisible par 400.


Écrire une fonction ``bissextile()`` qui prend en paramètre une année (représentée par un nombre entier) qui qui indique si cette année est bissextile.

un nombre entier représentant uneérifie qu'une liste de nombres est en ordre croissant.

.. TODO easypython:: bissextile.py
   :language: python
   :uuid: 1231313

.. easypython:: ./bissextile/
   :language: Jacadi
   :titre: bissextile
   :extra_yaml:
     fichier_ens: bissextile.py

