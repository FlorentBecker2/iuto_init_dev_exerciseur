Moyenne pondérée
---------------------------------

Écrire une fonction ``moyenne(notes, coefficients)`` qui prend en paramètre deux listes :

* une liste de notes 
* la liste des coefficients

Cette fonction devra renvoyer la moyenne des notes pondérées par les coefficients donnés.


.. easypython:: ./moyenne_ponderee/
   :language: Jacadi
   :titre: moyenne_ponderee
   :extra_yaml:
     fichier_ens: moyenne_ponderee.py
