entrees_visibles=[
        ([1, 2, 4, 8, 20, 50],2),
        ([1, 2, 4, 8, 20, 50],6),
        ([1, 2, 4, 8, 20, 50],9),
]
entrees_invisibles=[
        ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10],0),
        ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10],21),
        ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10],5),
        ([],0),
        ([],5),
]

@solution
def somme_partielle(liste,n):
  return sum(liste[:n])

