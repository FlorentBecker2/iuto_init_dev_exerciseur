entrees_visibles=[
        ('e', "coucou tout le monde",),
        ('z', "coucou",),
        ("a", "Eh ! J'adore l'informatique"),
]
entrees_invisibles=[
        ('a', ""),
        ('e', "jdbazebEEbeeeeee"),
        (" ", "rtz  bv g ht  q q s d fff "),
]


@solution
def compte_lettre(lettre, chaine):
  return chaine.count(lettre)

