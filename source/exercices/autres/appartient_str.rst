:orphan:

Une lettre appartient à un mot
------------------------------

Ecrire une fonction permettant de savoir si une lettre appartient à un mot. 


.. easypython:: ./appartient_str/
   :language: Jacadi
   :titre: appartient_str
   :extra_yaml:
     fichier_ens: appartient_str.py
