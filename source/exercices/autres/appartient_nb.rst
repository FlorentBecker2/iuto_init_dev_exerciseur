:orphan:

Un nombre appartient à une liste
--------------------------------

Ecrire une fonction permettant de savoir si un nombre appartient à une liste. 

.. easypython:: ./appartient_nb/
   :language: Jacadi
   :titre: appartient_nb
   :extra_yaml:
     fichier_ens: appartient_nb.py
