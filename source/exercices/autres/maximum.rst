:orphan:

Maximum
-------

Ecrire une fonction renvoie le plus grand nombre d'une liste de nombres. 

.. easypython:: ./maximum/
   :language: Jacadi
   :titre: maximum
   :extra_yaml:
     fichier_ens: maximum.py
