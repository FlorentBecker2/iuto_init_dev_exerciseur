:orphan:

.. image:: /images/girafe.png
   :align: left
   :height: 4em


Le plus d'animaux
-----------------

On dispose d'un dictionnaire dont les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux.
Par exemple :

``animaux={'éléphant': 'Asie', 'écureuil': 'Europe', 'panda': 'Asie', 'hippopotame': 'Afrique', 'girafe': 'Afrique', 'iguane': 'Amérique', 'lion': 'Afrique'}``

Ecrire une fonction qui, à partir d'un tel dictionnaire,
détermine le continent ayant le plus grand nombre d'espèces recensées.

.. easypython:: ./lePlusDAnimaux/
   :language: Jacadi
   :titre: Animaux Le plus d'animaux
   :tags:
     - animaux
   :extra_yaml:
     fichier_ens: lePlusDAnimaux.py


