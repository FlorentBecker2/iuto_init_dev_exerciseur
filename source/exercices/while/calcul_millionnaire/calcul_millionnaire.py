entrees_visibles=[
        10000,
        2000,
        11000000,
]

entrees_invisibles=[ 
        100,
]

@solution
def calcul_millionnaire(capital_de_depart):
  n=0
  while capital_de_depart < 1000000:
    n=n+1
    capital_de_depart*=1.05
  return n
  

