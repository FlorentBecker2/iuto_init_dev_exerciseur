entrees_visibles=[
        14,
        22,
        5,
]

entrees_invisibles=[ 
        1,
        2,
        3,
        4
]

@solution
def syracuse_suite(nombre):
  def suivant(nombre):
    if nombre%2 ==0:
      return nombre //2
    else:
      return nombre*3+1
  
  suite=[nombre]
  while nombre!=1:
    nombre=suivant(nombre)
    suite.append(nombre)
  return suite
  
