:orphan:

Cryptage d'un texte
-------------------

En utilisant la fonction *codeLettre*, écrire une fonction *crypter* qui
permet de decrypter un texte suivant une clé donnée.
**ATTENTION** n'oubliez pas de recopier le code de la fonction *codeLettre*


.. easypython:: ./crypter/
   :language: Jacadi
   :titre: frequence
   :extra_yaml:
     fichier_ens: crypter.py
