entrees_visibles = [('bonjour','nbvcxwmlkjhgfdsqpoiuy treza'),
                    ('le tp','nbvcxwmlkjhgfdsqpoiuy treza'),
                    ('au revoir','nbvcxwmlkjhgfdsqpoiuy treza')
]
entrees_invisibles = [
        ('dslkfj slmkdfj lkve','nbvcxwmlkjhgfdsqpoiuy treza'),
        ('azertyuiopqsdfghjklmwxcvbn ','nbvcxwmlkjhgfdsqpoiuy treza'),
        ('scjn sdjh mlsqjh','poirezuy tnwmlkjhgfbvcxdsqa')
]

@solution
def crypter(texte,cle):
    res=''
    for lettre in texte:
        code=codeLettre(lettre,cle)
        if code>=0:
            res+='0'+str(code)
    return res

def codeLettre(lettre, cle):
    res=-1
    for i in range(len(cle)):
        if cle[i]==lettre:
            res= i+1 + i//9
    return res
