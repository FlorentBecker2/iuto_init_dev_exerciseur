Parité d'un nombre entier
-------------------------------

Écrire une fonction ``estpair()`` qui prend en paramètre un nombre entier et qui indique si ce nombre est pair.


.. easypython:: ./estpair/
   :language: Jacadi
   :titre: estpair
   :extra_yaml:
     fichier_ens: estpair.py

.. image:: /images/defi.jpeg
   :height: 3em
   :align: left

Pouvez-vous écrire cette fonction sans le mot clef ``if`` ?
