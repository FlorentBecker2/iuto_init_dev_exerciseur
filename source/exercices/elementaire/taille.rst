Taille de la liste
-------------------------------

Écrire une fonction ``taille()`` qui prend en paramètre une liste et qui indique quelle est sa taille.


.. easypython:: ./taille/
   :language: Jacadi
   :titre: taille
   :extra_yaml:
     fichier_ens: taille.py

