La liste possède-t-elle un nombre plus petit que ... ?
-------------------------------------------------------

Écrire une fonction ```contient_nombre_inferieur(liste, seuil)``` qui prend deux paramètres :

* ``liste`` est une liste de nombres ;
* ``seuil`` est un nombre.


Cette fonction doit indiquer si oui ou non la liste contient au moins un nombre strictement inférieur à seuil.


.. easypython:: ./contient_nombre_inferieur/
   :language: Jacadi
   :titre: contient_nombre_inferieur
   :extra_yaml:
     fichier_ens: contient_nombre_inferieur.py

