La liste est-elle triée ?
---------------------------

Écrire une fonction ```liste_est_triee()``` qui vérifie qu'une liste de nombres est correctement triée par ordre croissant.


.. easypython:: ./liste_est_triee/
   :language: Jacadi
   :titre: liste_est_triee
   :extra_yaml:
     fichier_ens: liste_est_triee.py
