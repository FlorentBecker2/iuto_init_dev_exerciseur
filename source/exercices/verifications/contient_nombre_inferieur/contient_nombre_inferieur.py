entrees_visibles=[
        ([15, 2, 3, 9, 71, 5, 3], 10),
        ([15, 12, 5, 9, 71, 5, 13], 3),
        ([-9, -1, -5], -10),
        ([-9, -1, -5], 4),
]
entrees_invisibles=[
        ([1, 12, 3, 9, 71, 5, 3], 2),
        ([15, 2, 13, 9, 71, 15, 13], 5),
        ([15, 12, 13, 9, 71, 15, 3], 5),
        ([15, 2, 3, 9, 71, 5, 3], -3),
        ([15, 2, 3, 9, 71, 5, 3], 90),
]

@solution
def contient_nombre_inferieur(liste, seuil):
  return min(liste) < seuil

