entrees_visibles=[
        [6,8,47,89],
        [8, 9, 5, 7, 14],
        [],
]
entrees_invisibles=[
        [1, 12, 23, 34, 55, 76],
        [17, 12, 23, 34, 55, 76],
        [1, 12, 23, 34, 55, 6],
]

@solution
def liste_est_triee(liste):
  for i in range(1,len(liste)):
    if liste[i-1]>liste[i]:
      return False
  return True

