origines2 = {'éléphant':'Afrique', 'panda':'Asie', 'hippopotame':'Afrique', 'girafe':'Afrique', 'lion':'Afrique'}
origines1 = {'éléphant':'Asie', 'écureuil':'Europe', 'panda':'Asie', 'hippopotame':'Afrique', 'girafe':'Afrique', 'iguane':'Amérique', 'lion':'Afrique'}



a={'a': 1, 'b': 1, 'c': 1, 'd': 1, 'e': 1, 'f': 0}
r={'a':0, 'b':2, 'c':32, 'd':0, 'e':2, 'f':32}


entrees_visibles = [ origines1, origines2 ]
entrees_invisibles = [a, r]


@solution
def resensement(origines):
    res = {}
    for continent in origines.values():
        res[continent] = res.get(continent,0) + 1
    return res

#  for e in entrees_visibles+entrees_invisibles:
    #  print(resensement(e))


