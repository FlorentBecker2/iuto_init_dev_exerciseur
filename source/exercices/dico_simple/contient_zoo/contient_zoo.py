beauval={'éléphant': 15, 'écureuil': 78, 'panda': 3, 'lion': 12}
laval={'éléphant': 7, 'mésange': 41, 'hippopotame': 5, 'lion': 7}

a={'a': 1, 'b': 3, 'c': 5, 'd': 2, 'e': 12, 'f': 22}
r={'a':10, 'b':12, 'c':11, 'd':18, 'e':16, 'f':36}


entrees_visibles = [ beauval, laval ]
entrees_invisibles = [a, r]



@solution
def contient_espece_rare(zoo):
    for nb in zoo.values():
        if nb <= 4:
            return True
    return False

#  for e in entrees_visibles+entrees_invisibles:
    #  print(contient_espece_rare(e))


