Dictionnaire et animaux
---------------------------------


On dispose de dictionnaires modélisant ne nombre d'animaux de chaque espèce dans des zoos (les clefs sont les noms des animaux les valeurs le nombre de représentant de chaque espèce dans le zoo). Par exemple :

.. code-block:: python

   beauval={'éléphant': 15, 'écureuil': 78, 'panda': 3, 'lion': 12}
   laval={'éléphant': 7, 'mésange': 41, 'hippopotame': 5, 'lion': 7}


**Question 1.** Écrire une fonction ``nombre_total`` qui prend un zoo en paramètre et qui renvoie le nombre d'animaux que contient le zoo au total.


.. easypython:: ./total_zoo/
   :language: Jacadi
   :titre: total_zoo
   :extra_yaml:
     fichier_ens: total_zoo.py


**Question 2.** Ecrire une fonction ``contient_espece_rare`` qui prend un zoo en paramètre et qui vérifie si le zoo possède au moins une espèce avec 4 représentants ou moins.


.. easypython:: ./contient_zoo/
   :language: Jacadi
   :titre: contient_zoo
   :extra_yaml:
     fichier_ens: contient_zoo.py


**Question 3.** Écrire une fonction ``espece_max`` qui prend un zoo en paramètre et qui renvoie le nom de l'espèce la plus représentée dans ce zoo.


.. easypython:: ./max_zoo/
   :language: Jacadi
   :titre: max_zoo
   :extra_yaml:
     fichier_ens: max_zoo.py


**Question 4.** **Dictionnaire des fréquences**

On dispose d'un nouveau dictionnaire dont les clefs sont des noms des animaux, les valeurs sont les continents d'origine de ces animaux.


.. code-block:: python

   origines = {'éléphant':'Asie', 'écureuil':'Europe', 'panda':'Asie', 'hippopotame':'Afrique', 'girafe':'Afrique', 'iguane':'Amérique', 'lion':'Afrique'}


Écrire une fonction ``resencement`` qui prend un tel dictionnaire en paramètre et qui renvoie un dictionnaire dont les clefs sont les continents et les valeurs le nombre d'espèces resencées dans ce continent.


.. easypython:: ./dico_freq_zoo/
   :language: Jacadi
   :titre: dico_freq_zoo
   :extra_yaml:
     fichier_ens: dico_freq_zoo.py
   

