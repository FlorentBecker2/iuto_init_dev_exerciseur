beauval={'éléphant': 15, 'écureuil': 78, 'panda': 3, 'lion': 12}
laval={'éléphant': 7, 'mésange': 41, 'hippopotame': 5, 'lion': 7}

a={'a': 1, 'b': 3, 'c': 5, 'd': 2, 'e': 12, 'f': -18}
r={'a':0, 'b':2, 'c':32, 'd':8, 'e':16, 'f':4}


entrees_visibles = [ beauval, laval ]
entrees_invisibles = [a, r]



@solution
def espece_max(groupe):
    return max(groupe.keys(), key= lambda nom:groupe[nom])

#  for e in entrees_visibles+entrees_invisibles:
    #  print(espece_max(e))


