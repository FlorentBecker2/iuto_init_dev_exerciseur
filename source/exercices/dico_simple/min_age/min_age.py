groupeA = {"Albert":17, "Bernard":14, "Chloé":17, "David":17, "Isidore":13}
groupeB = {"Farid":19, "Gérard":16, "Hernestine":18, "Justine":18}

a={'a': 1, 'b': 3, 'c': 5, 'd': 2, 'e': 12, 'f': -18}
r={'a':0, 'b':2, 'c':32, 'd':8, 'e':16, 'f':4}


entrees_visibles = [ groupeA, groupeB ]
entrees_invisibles = [a, r]


@solution
def min_age(groupe):
    return min(groupe.keys(), key= lambda nom:groupe[nom])

#  for e in entrees_visibles+entrees_invisibles:
    #  print(min_age(e))


