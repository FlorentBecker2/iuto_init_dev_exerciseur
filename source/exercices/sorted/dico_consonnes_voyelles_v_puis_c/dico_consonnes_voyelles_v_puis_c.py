from hypothesis import given, example
import hypothesis.strategies as st
from fail_test import fail_test
import unicodedata

attendu_etudiant = [ 'voyelles_puis_consonnes' ]

voyelles = 'aeiouyœæøəɵƣʊⱥʌ'

latin_text = st.text(alphabet=st.characters(whitelist_categories='L', max_codepoint=0x24F))

def lettre_base(c):
    """
    :param c: une lettre de l'alphabet latin (éventuellement accentuée)
    :return: la lettre de base (en minuscule, sans accents et autres)
    :Example:

    >>> lettre_base('Ç')
    'ç'
    """
    nfd = unicodedata.normalize('NFD', c)
    return nfd[0].lower()

def est_voyelle_latine(c):
    """
    'c' est une lettre de l'alphabet latin
    """
    c = lettre_base(c)
    return c in voyelles

def construit_dico(lettres):
    return {l : est_voyelle_latine(l) for l in lettres}


alphabet_latin = []
for i in range(0x24F):
    l = chr(i)
    if unicodedata.category(l)[0] == 'L':
        alphabet_latin.append(l)

latin_text = st.text(alphabet=alphabet_latin)


@given(latin_text)
@example('abcdefghi')
def test_type(code_etu, lettres):
    d = construit_dico(lettres)
    sa_sortie = code_etu.voyelles_puis_consonnes(d)
    if not isinstance(sa_sortie, list):
        fail_test("Sur l'entrée %r, vous retournez %r qui n'est pas une liste." % (d, sa_sortie))
    if any(not isinstance(x, tuple) for x in sa_sortie):
        fail_test("Sur l'entrée %r, vous retournez %r dont un élément n'est pas un tuple." % (d, sa_sortie))
    if any(not len(x) == 2 for x in sa_sortie):
        fail_test("Sur l'entrée %r, vous retournez %r dont un élément n'est pas un couple." % (d, sa_sortie))
    if any(not isinstance(x[0], str) for x in sa_sortie):
        fail_test("Sur l'entrée %r, vous retournez %r dont un élément n'a pas une chaîne comme première composante." % (d, sa_sortie))
    if any(not isinstance(x[1], bool) for x in sa_sortie):
        fail_test("Sur l'entrée %r, vous retournez %r dont un élément n'a pas une booléen comme deuxième composante." % (d, sa_sortie))

@given(latin_text)
@example('abcdefghi')
def test_en_ordre_c_puis_v(code_etu, lettres):
    d = construit_dico(lettres)
    sa_sortie = code_etu.voyelles_puis_consonnes(d)
    n = len(sa_sortie)
    indices_voyelles = set()
    indices_consonnes = set()
    for i in range(n):
        if sa_sortie[i][1]:
            indices_voyelles.add(i)
        else:
            indices_consonnes.add(i)
    v_max = max(0, 0, *indices_voyelles)
    c_min = min(n, n, *indices_consonnes)
    if v_max > c_min:
        fail_test("Sur l'entrée %r,\n vous retournez %r,\n où %r (une consonne) est avant %r (une voyelle)" % (d, sa_sortie, sa_sortie[c_min], sa_sortie[v_max]))

@given(latin_text)
@example('abcdefghi')
def test_toutes_lettres(code_etu, lettres):
    d = construit_dico(lettres)
    sa_sortie = code_etu.voyelles_puis_consonnes(d)
    son_ensemble = set(sa_sortie)
    mon_ensemble = set(d.items())
    elems_en_trop = son_ensemble - mon_ensemble
    elems_manquants = mon_ensemble - son_ensemble
    if len(elems_en_trop) > 0:
        fail_test("Sur l'entrée %r, vous retournez %r, où il manque les éléments %r" % (d, sa_sortie, elems_manquants))
    if len(elems_en_trop) > 0:
        fail_test("Sur l'entrée %r, vous retournez %r, où les éléments %r sont en trop" % (d, sa_sortie, elems_en_trop))

@given(lettres=latin_text)
@example('abcdefghi')
def test_categorisation(code_etu, lettres):
    d = construit_dico(lettres)
    sa_sortie = code_etu.voyelles_puis_consonnes(d)
    for (lettre, cat) in sa_sortie:
        if cat != d[lettre]:
            fail_test("Sur l'entrée %r, vous retournez %r; la valeur associée à %r est %r au lieu de %r" % (d, sa_sortie, lettre, cat, d[lettre]))
