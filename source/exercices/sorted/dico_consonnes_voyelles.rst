Dictionnaires Voyelles / Consonnes
----------------------------------

On dispose d'un dictionnaire de la forme `d = {'a': True, 'b': False, 'y': True, 'g': False}` qui indique pour des lettres si ce sont des consonnes ou des voyelles. On veut afficher ces lettres avec leur nature (consonne ou voyelle) dans un certain ordre. Pour cela, on va mettre les couples (lettre, nature) dans une liste, dans l'ordre où on veut les afficher.

Par exemple, à partir du dictionnaire précédent, on peut vouloir l'affichage suivant:

.. code-block:: text

   a est une voyelle
   b est une consonne
   g est une consonne
   y est une voyelle



On va alors créer la liste suivante `[('a', True), ('b': False), ('g': False), ('y', True)]`, puis appeler la fonction ci-dessous:

.. code-block:: python

  def affiche_liste_consonnes_voyelles(liste_lettre_nature):
    for (lettre, est_voyelle) in liste_lettre_nature:
      if est_voyelle:
        print(lettre, 'est une voyelle')
      else:
        print(lettre, 'est une consonne')

  affiche_liste_consonnes_voyelles([('a', True), ('b': False), ('g': False), ('y', True)])


On vous demande d'écrire ci-dessous des fonctions pour créer ces listes à partir d'un dictionnaire.

Écrire une fonction `en_ordre_alpha` qui met dans une liste les couples (clé, valeur) du dictionnaire, *dans l'ordre alphabétique des clés*.

.. easypython:: ./dico_consonnes_voyelles_alpha/
   :language: TestsPython
   :titre: dico_consonnes_voyelles_alpha
   :extra_yaml:
     fichier_ens: dico_consonnes_voyelles_alpha.py


On veut maintenant d'abord les voyelles, puis les consonnes. Écrire une fonction `voyelles_puis_consonnes` à cet effet. *Indication: python considère que `True > False`*

.. easypython:: ./dico_consonnes_voyelles_v_puis_c/
   :language: TestsPython
   :titre: dico_consonnes_voyelles_v_puis_c
   :extra_yaml:
     fichier_ens: dico_consonnes_voyelles_v_puis_c.py


On veut maintenant d'abord les voyelles, puis les consonnes, *par ordre alphabétique dans chaque catégorie*. Écrire une fonction `voyelles_puis_consonnes` à cet effet.

.. easypython:: ./dico_consonnes_voyelles_lexico/
   :language: TestsPython
   :titre: dico_consonnes_voyelles_lexico
   :extra_yaml:
     fichier_ens: dico_consonnes_voyelles_lexico.py
