Des montagnes
---------------------------------


On décide de représenter une montagne sous la forme d'un tuple ``(nom, altitude)``. Par exemple :

.. code-block:: python

   kili = ("Kilimandjaro", 5892)
   mont_blanc = ("Mont Blanc", 4809)



**Question 1.** Écrire une fonction ``moyenne_altitude`` qui prend une liste de montagnes en paramètre et qui renvoie la moyenne des altitudes des montagnes de la liste.


.. easypython:: ./moyenne_montagne/
   :language: Jacadi
   :titre: moyenne_montagne
   :extra_yaml:
     fichier_ens: moyenne_montagne.py


**Question 2.** Écrire une fonction ``contient_sommet_exceptionnel`` qui prend une liste de montagnes en paramètre et qui indique si cette liste contient un sommet exceptionnel, c'est à dire une montagne dont l'altitude est de 6000 mètres ou plus.


.. easypython:: ./contient_montagne/
   :language: Jacadi
   :titre: contient_montagne
   :extra_yaml:
     fichier_ens: contient_montagne.py


**Question 3.** Écrire une fonction ``plus_haute_montagne`` qui prend une liste de montagnes en paramètre et qui renvoie le nom de la montagne la plus haute de la liste.


.. easypython:: ./max_montagne/
   :language: Jacadi
   :titre: max_montagne
   :extra_yaml:
     fichier_ens: max_montagne.py


**Question 4.** **Dictionnaire des fréquences**

Écrire une fonction ``index_alphabetique`` qui prend liste de montagnes en paramètre et qui renvoie un dictionnaire dont les clefs sont des lettres et les valeurs, le nombre de montagnes dont le nom commence par cette lettre.


.. easypython:: ./dico_freq_montagne/
   :language: Jacadi
   :titre: dico_freq_montagne
   :extra_yaml:
     fichier_ens: dico_freq_montagne.py

