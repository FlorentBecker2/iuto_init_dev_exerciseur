trois_amis = {
    'Camille' : {'Vélo', 'Kayak', 'Boxe'},
    'Dominique' : {'Vélo','Boxe'},
    'Claude' : {'Lecture', 'Tricot', 'Boxe'}
    }

autre = {
    'a' : {'1', '2', '3'},
    'b' : {'4'},
    'c' : {'5', '6', '4'}
    }
    
    
entrees_visibles = [ trois_amis ]
entrees_invisibles = [ autre ]

def dico_freq(amis):
    res = {}
    for amis , ensemble  in amis.items():
        for activite in ensemble:
            if activite in res:
                res[activite].add(amis)
            else:
                res[activite]= {amis}
    return res
    

@solution
def activite_la_plus_populaire(amis):
    dico = dico_freq(amis)
    return max(dico, key=lambda x:dico[x])

#  for p in entrees_visibles+entrees_invisibles:
    #  print(activite_la_plus_populaire(p))


