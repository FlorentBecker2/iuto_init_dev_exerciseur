afrique = [ ('Silki', 4420), ('Kilimandjaro', 5892), ('Mont Cameroun', 4040)]
asie = [('Trivor', 7577), ('Annapurna I', 8091), ('Everest', 8848), ('K2', 8611)]

a=[('a', 1), ('b', 3), ('c', 6000), ('d', 2), ('e', 12), ('f', -18)]
r=[('a',100), ('b',2), ('c',32), ('d',8), ('e',16), ('f',4)]


entrees_visibles = [ afrique, asie ]
entrees_invisibles = [a, r]


@solution
def plus_haute_montagne(liste):
    return max(liste, key= lambda x:x[1])[0]

#  for e in entrees_visibles+entrees_invisibles:
    #  print(plus_haute_montagne(e))


