Une activité pour ce soir
---------------------------------

On représente un groupe de personne par un dictionnaire dont les clés sont les noms des participants, et les valeurs sont des ensembles d'activités qu'elles apprécient.

Voici un exemple d'un groupe de 3 amis:

.. code-block:: python

  trois_amis = {
    'Camille' : {'Vélo', 'Kayak', 'Boxe'},
    'Dominique' : {'Vélo'},
    'Claude' : {'Lecture', 'Tricot', 'Boxe'}
  }


**Question 1.** Ecrire une fonction ``ensemble_activite`` qui à partir d'un groupe d'amis, renvoie l'ensemble des activités appréciées par les participants.


.. easypython:: ./compte_activite/
   :language: Jacadi
   :titre: compte_activite
   :extra_yaml:
     fichier_ens: compte_activite.py


**Question 2.**  Donner une fonction ``amis_difficiles`` qui à partir d'un groupe d'amis, renvoie ``True`` s'il existe au moins un participant qui a une seule activité favorite, et ``False`` sinon.


.. easypython:: ./contient_activite/
   :language: Jacadi
   :titre: contient_activite
   :extra_yaml:
     fichier_ens: contient_activite.py


**Question 3.** Écrire une fonction ``popularite_activite`` qui prend un groupe d'amis en paramètre et qui renvoie un dictionnaire dont les clefs sont les noms des activités, et les valeurs l'ensemble des noms des amis qui apprécient cette activité.


.. easypython:: ./dico_freq_activite/
   :language: Jacadi
   :titre: dico_freq_activite
   :extra_yaml:
     fichier_ens: dico_freq_activite.py


**Question 4.** Écrire une fonction ``activite_la_plus_populaire`` qui prend un groupe d'amis en paramètre et qui renvoie le nom de l'activité la plus appréciée


.. easypython:: ./max_activite/
   :language: Jacadi
   :titre: max_activite
   :extra_yaml:
     fichier_ens: max_activite.py


**Question 5.** Écrire une fonction ``activite_la_moins_populaire`` qui prend un groupe d'amis en paramètre et qui renvoie le nom de l'activité la moins appréciée


.. easypython:: ./min_activite/
   :language: Jacadi
   :titre: min_activite
   :extra_yaml:
     fichier_ens: min_activite.py
