entrees_visibles = [[12,4,5,6,17],
                    [-2,-24,-14,-8,-19,-7],
                    [-2,24,-14,8,19,-7],
]
entrees_invisibles = [
                    [202,24,14,8,19,-7],
                    [-9999999999],
                    [-9999999999, -555555555555],
]

@solution
def minimum_positif(liste):
  l=[n for n in liste if n>=0]
  if l==[]:
    return None
  else:
    return min(l)
