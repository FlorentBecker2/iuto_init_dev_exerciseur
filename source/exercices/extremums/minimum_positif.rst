Minimum positif
--------------------

Ecrire une fonction renvoie le plus petit nombre **positif** d'une liste de nombres.
Attention, si ce minimum n'existe pas, cette fonction devra renvoyer ``None``


.. easypython:: ./minimum_positif/
   :language: Jacadi
   :titre: minimum_positif
   :extra_yaml:
     fichier_ens: minimum_positif.py
