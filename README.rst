Des exercices interactifs en Python pour les étudiants de BUT1 à l'IUT Informatique d'Orléans
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Cloner le projet :
====================

* Installer virtualenv

* Cloner le dépot dans un répertoire dédié :

  ``git clone https://gitlab.com/adobet/iuto_init_dev_exerciseur.git .``


* Créer un virtualenv et l'activer :

  ``virtualenv -p python3 venv``
  
  ``source venv/bin/activate``


* Installer les dépendances :

  ``(venv)$ pip install -r requirements.txt``


* Compiler :

  ``make html``
 

Créer un projet sur ce modèle : 
================================

* Installer virtualenv

* Cloner le dépot "mère" dans un répertoire dédié :

  ``git clone https://gitlab.com/jrobert/easySphinx.git .``


* Créer un virtualenv et l'activer :

  ``virtualenv -p python3 venv``
  
  ``source venv/bin/activate``


* Installer les dépendances :

  ``(venv)$ pip install -r requirements.txt``


* Compiler :
    
  ``make html``


* Créer un dépot **PUBLIC** sur GitLab, puis (toujours dans le répertoire dédié) :

  ``git remote rename origin old-origin``
  
  ``git remote add origin https://gitlab.com/monnom/mondepot.git``

  ``git push -u origin --all``

  ``git push -u origin --tags``


Faire éventuellement les modifs suivantes :

* Dans le `requirements.txt`, ajouter les lignes
   
     ``sphinx-rtd-theme==0.4.0``
     
     ``hypothesis``
     
* Dans le .gitignore, ajouter ``venv/``
   
* Dans le fichier `source/conf.py`, à la ligne 78, mettre ``sphinx_rtd_theme`` à la place de ``alabaster`` et mettre à jour les infos.
